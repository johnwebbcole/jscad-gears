import test from 'ava';
import Gear from '../src/Gear';

import { csgImageSnapshot } from '@jwc/jscad-test-utils';

test('import Gear', t => {
  t.snapshot(Object.keys(Gear).sort());
});

test('create a gear', async t => {
  var qualitySettings = {
    resolution: CSG.defaultResolution2D,
    stepsPerToothAngle: 3
  };
  var wheel = { id: 50, od: 60, h: 20 };

  var toothCount = 32;

  var g = new Gear({
    circularPitch: (Math.PI * (wheel.id + 4)) / toothCount,
    toothCount: toothCount,
    qualitySettings: qualitySettings
  });

  var shape = g.getZeroedShape();
  var gear = util.poly2solid(shape, shape, 15);

  const value = await csgImageSnapshot(t, gear);
  t.true(value);
});

test('create an internal gear', async t => {
  var wheel = { id: 50, od: 60, h: 20 };

  var toothCount = 96;
  var circularPitch = (Math.PI * (wheel.id + 4)) / toothCount;

  var p = new Gear({
    circularPitch,
    toothCount: 14
  });

  var g = new Gear({
    circularPitch: (Math.PI * (wheel.id + 4)) / toothCount,
    toothCount: -toothCount,
    connectedGear: p
  });

  var shape = g.getZeroedShape();
  var gear = util.poly2solid(shape, shape, 15);

  const value = await csgImageSnapshot(t, gear);
  t.true(value);
});

test('create a rack gear', async t => {
  var toothCount = 0;

  var g = new Gear({
    circularPitch: (Math.PI * 54) / 32,
    toothCount: toothCount
  });

  var shape = g.getZeroedShape();
  var gear = util.poly2solid(shape, shape, 15);

  const value = await csgImageSnapshot(t, gear);
  t.true(value);
});
