import test from 'ava';
import GearSet from '../src/GearSet';
import Gear from '../src/Gear';
import { csgImageSnapshot } from '@jwc/jscad-test-utils';

test('import GearSet', t => {
  t.snapshot(Object.keys(GearSet).sort());
});

test('create a GearSet', async t => {
  var mainPitchDiameter = 50;
  var mainToothCount = 96;
  var pinionToothCount = 14;

  var toothCount = mainToothCount;
  var circularPitch = (Math.PI * mainPitchDiameter) / toothCount;

  var pinion = new Gear({
    circularPitch,
    toothCount: pinionToothCount
  });

  var gear = new Gear({
    circularPitch,
    connectedGear: pinion,
    toothCount: toothCount
  });

  var gearset = new GearSet(gear, pinion, 3);

  var shape = gearset.createShape();
  var solid = util.poly2solid(shape, shape, 10);

  const value = await csgImageSnapshot(t, solid);
  t.true(value);
});

test('create a ring GearSet', async t => {
  var mainPitchDiameter = 50;
  var mainToothCount = 96;
  var pinionToothCount = 14;

  var toothCount = mainToothCount;
  var circularPitch = (Math.PI * mainPitchDiameter) / toothCount;

  var pinion = new Gear({
    circularPitch,
    toothCount: pinionToothCount
  });

  var gear = new Gear({
    circularPitch,
    connectedGear: pinion,
    toothCount: -toothCount
  });

  var gearset = new GearSet(gear, pinion, 3);

  var shape = gearset.createShape();
  var solid = util.poly2solid(shape, shape, 10);

  const value = await csgImageSnapshot(t, solid);
  t.true(value);
});
