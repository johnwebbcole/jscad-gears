[![view on npm](http://img.shields.io/npm/v/jscad-gears.svg)](https://www.npmjs.org/package/jscad-gears) [![npm module
downloads](http://img.shields.io/npm/dt/jscad-gears.svg)](https://www.npmjs.org/package/jscad-gears)

# jscad-gears

![gear example](./images/jscad-gears.png)

This library creates JsCad gears, it is based on the [Involute Spur Gear Builder](http://hessmer.org/gears/InvoluteSpurGearBuilder.html) . This library uses the
[jscad-utils](https://github.com/johnwebbcole/jscad-utils) library and return jscad-utils [`group`
objects](https://github.com/johnwebbcole/jscad-utils#utilgroupnames-objects--object).

## Installation

Install `jscad-gears` using NPM:

```bash
npm install --save jscad-gears jscad-utils
```

## Basic usage

To use the gears package, you need to include the `jscad-gears.jscad` file and `jscad-utils.jscad`.

```javascript
main() {
util.init(CSG);

var qualitySettings = {
resolution: CSG.defaultResolution2D,
stepsPerToothAngle: 3
};
var wheel = { id: 78.5, od: 104.1, h: 22 };

var toothCount = 64;

var g = new Gear({
toothCount,
circularPitch: (Math.PI * (wheel.id + 4)) / toothCount,
toothCount: toothCount,
qualitySettings: qualitySettings
});

var shape = g.getZeroedShape();
return util.poly2solid(shape, shape, 15);
}
```

## Yeoman Generator

You can use the [yeoman jscad generator](https://www.npmjs.com/package/generator-jscad) which will create a project
that uses this library.

Once you create your project, install `jscad-gears`, and run `gulp`. Dragging the `dist` directory into
[http://openjscad.org/](http://openjscad.org/) will include this library.

Modify your `main.jscad` file to return a RaspberryPi object.

```javascript
// include:js
// endinject
/* exported main, getParameterDefinitions */
/* globals piexample */

function main(params) {
  util.init(CSG);

  var qualitySettings = {
    resolution: CSG.defaultResolution2D,
    stepsPerToothAngle: 3
  };
  var wheel = { id: 78.5, od: 104.1, h: 22 };

  var toothCount = 64;

  var g = new Gear({
    toothCount,
    circularPitch: (Math.PI * (wheel.id + 4)) / toothCount,
    toothCount: toothCount,
    qualitySettings: qualitySettings
  });

  var shape = g.getZeroedShape();
  return util.poly2solid(shape, shape, 15);
}
```

&copy; 2016 John Cole <johnwebbcole@gmail.com>.
