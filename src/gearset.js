import { Debug } from "@jwc/jscad-utils";
const debug = Debug("jscad-gears:gearset");
import { GearType } from "./gear";

/**
 * @function GearSet
 * @param  {type} gear1      {description}
 * @param  {type} gear2      {description}
 * @return {type} {description}
 */
function GearSet(gear1, gear2) {
  debug("GearSet", gear1, gear2);

  this.gear1 = gear1;
  gear1.connectedGear = gear2;
  this.gear2 = gear2;
  gear2.connectedGear = gear1;
  // in order for the two gears to mesh we need to turn the second one by 'half a tooth'
  //this.gear1.setAngle(0);
  this.gearRatio = this.gear1.toothCount / this.gear1.toothCount;

  var relativePitchRadius1 =
    this.gear1.gearType == GearType.Internal
      ? -this.gear1.pitchRadius
      : this.gear1.pitchRadius;
  var relativePitchRadius2 =
    this.gear2.gearType == GearType.Internal
      ? -this.gear2.pitchRadius
      : this.gear2.pitchRadius;
  this.gearsDistance = relativePitchRadius1 + relativePitchRadius2;
}

/**
 * @function {function name}
 * @return {type} {description}
 */
GearSet.prototype.createShape = function (showOption = 3) {
  var shape = new CAG();
  if ((showOption & 1) > 0) {
    // show gear 1
    var gear1Shape = this.gear1.getZeroedShape();
    //var gear1Shape = this.gear1.createCutoutDemo();
    shape = shape.union(gear1Shape);
  }
  if ((showOption & 2) > 0) {
    // show gear 2
    var gear2Shape = this.gear2.getZeroedShape();

    if (this.gear2.gearType == GearType.Regular) {
      // we need an angle offset of half a tooth for the two gears to mesh
      var angle = 180 + 180 / this.gear2.toothCount;
      // apply gear rotation
      gear2Shape = gear2Shape.rotateZ(angle);
    } else if (this.gear2.gearType == GearType.Internal) {
      // we need an angle offset of half a tooth for the two gears to mesh
      var angle = 180; // + 180 / this.gear2.toothCount;
      // apply gear rotation
      gear2Shape = gear2Shape.rotateZ(angle);
    } else if (this.gear2.gearType == GearType.Rack) {
      gear2Shape = gear2Shape.rotateZ(180);
      gear2Shape = gear2Shape.translate([0, this.gear2.circularPitch / 2]);
    }

    // move to correct center
    gear2Shape = gear2Shape.translate([this.gearsDistance, 0]);

    //var gear2Shape = this.gear2.createCutoutDemo();
    shape = shape.union(gear2Shape);
  }
  return shape;
};

export default GearSet;
