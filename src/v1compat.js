/* eslint-disable */
var Gear, GearSet;
function initJscadGears() {
  var Debug = util.Debug;
  var debug = Debug('JscadGears:initJscadGears');
  var jscadUtils = { util, Debug, parts: Parts, Group };
  // include:compat
  // end:compat

  debug('JscadGears', jscadGears);
  Gear = jscadGears.Gear;
  GearSet = jscadGears.GearSet;
}

/**
 * Add `initJscadGears` to the init queue for `util.init`.
 */
jscadUtilsPluginInit.push(initJscadGears);
/* eslint-enable */
