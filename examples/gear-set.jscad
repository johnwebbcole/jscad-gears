function main(params) {
  util.init(CSG, { debug: 'jscad-gears*' });

  var mainPitchDiameter = 50;
  var mainToothCount = 96;
  var pinionToothCount = 14;

  var toothCount = mainToothCount;
  var circularPitch = (Math.PI * mainPitchDiameter) / toothCount;

  var pinion = new Gear({
    circularPitch,
    toothCount: pinionToothCount
  });

  var gear = new Gear({
    circularPitch,
    connectedGear: pinion,
    toothCount: -toothCount
  });

  var gearset = new GearSet(gear, pinion, 3);

  var shape = gearset.createShape();
  return util.poly2solid(shape, shape, 10);
}

// include:js
// endinject
