function main(params) {
  util.init(CSG);

  var qualitySettings = {
    resolution: CSG.defaultResolution2D,
    stepsPerToothAngle: 3
  };
  var wheel = { id: 78.5, od: 104.1, h: 22 };

  var toothCount = 64;

  var g = new Gear({
    toothCount,
    circularPitch: (Math.PI * (wheel.id + 4)) / toothCount,
    toothCount: toothCount,
    qualitySettings: qualitySettings
  });

  var shape = g.getZeroedShape();
  return util.poly2solid(shape, shape, 15);
}

// include:js
// endinject
