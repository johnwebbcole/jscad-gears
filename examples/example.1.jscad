function main(params) {
  util.init(CSG);

  var qualitySettings = {
    resolution: CSG.defaultResolution2D,
    stepsPerToothAngle: 3
  };

  // D = Pitch Diameter
  // N = Number of teeth on the gear
  // P = Diametral Pitch (Gear Size)
  // D = N / P
  // P = N / D
  //
  // Outside Diameter (OD)
  // OD = N + 2 / P

  // Circular Pitch (CP)
  // CP = Math.PI * D / N = Math.PI / P
  var toothCount = 16;
  var circularPitch = Math.PI * 20 / toothCount;
  var gear1 = new Gear({
    circularPitch,
    pressureAngle: 20,
    clearance: 0.05,
    backlash: 0.05,
    toothCount,
    profileShift: -0,
    qualitySettings: qualitySettings
  });

  var shape = gear1.getZeroedShape();
  // console.log(shape.sides.reduce(function(result, side) {
  //   result.push([side.vertex0.pos.x, side.vertex0.pos.y]);
  //   result.push([side.vertex1.pos.x, side.vertex1.pos.y]);
  //   return result;
  // }, []).map(p => `[${p[0]},${p[1]}]`).join(','));
  return util
    .poly2solid(shape, shape, 5);
}

// include:js
// endinject
