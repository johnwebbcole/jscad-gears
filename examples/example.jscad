/* globals Gear */
/* exported main, getParameterDefinitions */

function getParameterDefinitions() {
  return [
    {
      name: 'resolution',
      type: 'choice',
      values: [0, 1, 2, 3, 4],
      captions: [
        'very low (6,16)',
        'low (8,24)',
        'normal (12,32)',
        'high (24,64)',
        'very high (48,128)'
      ],
      initial: 2,
      caption: 'Resolution:'
    },
    {
      type: 'group',
      name: 'maingear',
      caption: 'Main Gear'
    },
    {
      name: 'mainToothCount',
      caption: 'Tooth Count',
      type: 'int',
      initial: 64
    },
    {
      name: 'mainPitchDiameter',
      caption: 'Pitch Diameter',
      type: 'number',
      initial: 78.5
    },
    {
      type: 'group',
      name: 'pinion',
      caption: 'Pinion'
    },
    {
      name: 'pinionToothCount',
      caption: 'Tooth Count',
      type: 'int',
      initial: 14
    }
  ];
}

function main(params) {
  var resolutions = [[6, 16], [8, 24], [12, 32], [24, 64], [48, 128]];
  CSG.defaultResolution3D = resolutions[params.resolution][0];
  CSG.defaultResolution2D = resolutions[params.resolution][1];
  util.init(CSG, {debug:"*"});
  console.log('params', params);

  var qualitySettings = {
    resolution: CSG.defaultResolution2D,
    stepsPerToothAngle: 3
  };
  // var wheel = { id: 78.5, od: 104.1, h: 22 };
  var mainPitchDiameter = params.mainPitchDiameter;
  var toothCount = params.mainToothCount;
  var options = {
    // wheel,
    toothCount,
    circularPitch: (Math.PI * mainPitchDiameter) / toothCount,
    // hubThickness: 3.5,
    pinion: {
      toothCount: params.pinionToothCount
    }
  };

  var _gear = function({
    toothCount,
    circularPitch,
    profileShift = 0,
    connectedGear
  }) {
    // console.log('_gear', toothCount, circularPitch);
    return new Gear({
      circularPitch,
      connectedGear,
      pressureAngle: params.pressureAngle,
      clearance: params.clearance,
      backlash: params.backlash,
      toothCount,
      profileShift: profileShift,
      qualitySettings: qualitySettings
    });
  };

  var pinion = _gear(
    Object.assign({}, options, {
      toothCount: options.pinion.toothCount
    })
  );

  var gear = _gear(
    Object.assign(options, {
      connectedGear: pinion,
      toothCount: -toothCount,
      profileShift: 0
    })
  );

  var gearset = {
    gear,
    pinion
  };

  var standard = function(gear, h) {
    var shape = gear.getZeroedShape();
    return util.poly2solid(shape, shape, h);
  };

  console.log('mainPitchDiameter', mainPitchDiameter);
  console.log('gearset', gearset);
  return [
    standard(gearset.pinion, 10)
      .translate([gearset.gear.pitchRadius + gearset.pinion.pitchRadius, 0, 0])
      .color('orange'), //.rotateZ(360 / options.circularPitch),
    standard(gearset.gear, 10).color('blue')
    // CSG.cube({ radius: [mainPitchDiameter / 2, 1, 1] }).translate([0, 0, 11])
  ];
}

// include:js
// endinject
